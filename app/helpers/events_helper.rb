# Helper for the event views
module EventsHelper
  def set_events_meta
    set_meta_tags \
      description: t('layouts.application.subtitle'),
      keywords: @events.map(&:tags).join(', ').split.group_by { |i| i }
                       .reject { |_k, v| v.size < 2 }.map { |k, _v| k },
      DC: {
        title: t('layouts.application.title'),
        subject: t('layouts.application.subtitle'),
        publisher: 'april'
      }
  end

  def set_event_meta
    set_meta_tags \
      keywords: @event.tag_list,
      DC: { title: @event.title, date: @event.start_time.to_s },
      geo: {
        placename: @event.city,
        region: @event.region,
        position: "#{@event.latitude};#{@event.longitude}",
        ICBM: "#{@event.latitude}, #{@event.longitude}"
      }
  end

  def display_date(event = @event)
    if event.start_time.to_date == event.end_time.to_date
      display_sameday event
    else
      display_multi_days event
    end
  end

  def display_sameday(event)
    t 'date.formats.same_day',
      date: l(event.start_time.to_date, format: :long),
      start: l(event.start_time, format: :hours),
      end: l(event.end_time, format: :hours)
  end

  def display_multi_days(event)
    t 'date.formats.period',
      start: l(event.start_time, format: :at),
      end: l(event.end_time, format: :at)
  end

  def wrap(s, width = 78)
    s.gsub(/(.{1,#{width}})(\s+|\Z)/, "\\1\n")
  end

  # Select the events to display in a month, sorted
  def month_events(events, date)
    events.select { |e| (e.start_time.to_date..e.end_time.to_date).cover? date }
          .sort_by(&:city)
  end
end
