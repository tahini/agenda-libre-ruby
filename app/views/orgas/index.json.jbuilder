json.array!(@orgas) do |orga|
  json.extract! orga, :id, :name, :description,
                :place_name, :address, :city, :region_id, :url,
                :contact, :tag_list
  json.url orga_url(orga, format: :json)
end
