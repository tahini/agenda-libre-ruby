require 'test_helper'

# Event life cycle
class EventsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  setup do
    @event = events :one
  end

  test 'should get index' do
    get :index
    assert_response :success
    assert_not_nil assigns :events
  end

  test 'should get new' do
    get :new
    assert_response :success
  end

  test 'should preview event creation' do
    assert_no_difference 'Event.count' do
      post :preview_create, event: {
        title: @event.title,
        start_time: @event.start_time, end_time: @event.end_time,
        description: @event.description,
        city: @event.city, region_id: @event.region.id,
        url: @event.url, contact: @event.contact, tag_list: 'helo world'
      }

      assert_empty assigns(:event).errors
    end

    assert_response :success
  end

  test 'should create event' do
    assert_difference 'Event.count' do
      post :create, event: {
        title: @event.title,
        start_time: @event.start_time, end_time: @event.end_time,
        description: @event.description,
        city: @event.city, region_id: @event.region.id,
        url: @event.url,
        contact: @event.contact,
        tag_list: 'helo world'
      }

      assert_empty assigns(:event).errors.messages
    end

    assert_redirected_to :root
  end

  test 'should not create event' do
    assert_no_difference 'Event.count' do
      post :create, event: {
        title: @event.title, start_time: @event.start_time
      }

      assert_not_empty assigns(:event).errors.messages
    end
  end

  test 'should show event' do
    get :show, id: @event
    assert_response :success
  end

  test 'should get edit' do
    get :edit, id: @event, secret: @event.secret
    assert_response :success
  end

  test 'should not get edit' do
    get :edit, id: @event
    assert_redirected_to :root
  end

  test 'should preview' do
    assert_no_difference 'Event.count' do
      patch :preview, id: @event, secret: @event.secret, event: {
        title: @event.title
      }

      assert_empty assigns(:event).errors
    end

    assert_response :success
  end

  test 'should update event' do
    patch :update, id: @event, secret: @event.secret, event: {
      title: @event.title
    }

    assert_empty assigns(:event).errors.messages
    assert_redirected_to :root
  end

  test 'should not update event' do
    patch :update, id: @event, secret: @event.secret, event: { title: nil }

    assert_not_empty assigns(:event).errors.messages
  end

  test 'can not update event concurrently' do
    patch :update, id: @event, secret: @event.secret, event: {
      lock_version: @event.lock_version - 1
    }

    assert_redirected_to edit_event_url(@event, secret: @event.secret)
  end

  test 'should get cancel page' do
    get :cancel, id: @event, secret: @event.secret
    assert_response :success
  end

  test 'should destroy event' do
    assert_difference('Event.count', -1) do
      delete :destroy, id: @event, secret: @event.secret, event: {
        reason: 'bye'
      }
    end

    assert_equal 'bye', assigns(:event).reason
    assert_redirected_to :root
  end
end
