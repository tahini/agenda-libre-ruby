Geocoder.configure(
  # geocoding options
  # timeout: 3,           # geocoding service timeout (secs)
  lookup: :nominatim,     # name of geocoding service (symbol)
  language: :fr,          # ISO-639 language code
  http_headers: { 'User-Agent' => 'agendadulibre.org' }
)
