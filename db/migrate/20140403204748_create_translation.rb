# Add a translations table, to mange many texts from the database
class CreateTranslation < ActiveRecord::Migration
  def change
    create_table :translations do |t|
      t.string :locale
      t.string :key
      t.text :value
      t.text :interpolations
      t.boolean :is_proc, default: false
    end
  end
end
