# Adding an active flag to organisations, and a description
class AddActiveDescriptionToOrga < ActiveRecord::Migration
  def change
    add_column :orgas, :active, :boolean, null: false, default: true
    add_column :orgas, :description, :text
  end
end
